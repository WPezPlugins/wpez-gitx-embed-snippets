# WordPress Plugin: WPezGitXEmbedSnippets

__Embeds a GitLab snippet via shortcode. For details on additional magic please read below.__

This plugin is current a POC / WIP, please test thoroughly before using in production. Eventually, I'll refactor it but for now the priority was make it work. Truth be told, I scope creep'ed myself more than I should have. It's a nice set of features but all the details added up in terms of time. C'est la vie.  


### OVERVIEW

Benefits fall into three basic buckets: performance, privacy and usability.

- Under the hood, the plugin uses wp_remote_get() to retrieve a snippet's content and then stores that string in the WP post meta. This "conversion" happens during save_post, not during frontend activity. Ideally, this improves page load speed on the frontend because the snippet is "pre-fetched". 

- Using wp_remote_get() also adds a layer of privacy because the user's browser isn't making a request to GitLab for the snippet. 

- Aside from the snippet's markup, its CSS is also retrieved and stored in post meta. Currently, any CSS must be added to your theme (or elsewhere), as the functionality to use the CSS that is stored for the snippet is a TODO.

- The GitLab logo (.svg) is now served by the website/plugin, not GitLab. Again, this shields the user's browser from making a request to an external source.

- The shortcode allows you to define a list of which lines of code should be highlighted. See below.

- When the snippet is displayed on the frontend you can also customize the highlighting by clicking on the line numbers. The Share URL button has a query string arg that stores the highlighted settings so you can share your customized highlighting.

- Note: Since we're storing the snippet in the WordPress DB, if you update the snippet on GitLab, you'll then have to go back to WordPress and re-save the posts/pages where that snippet is being embedded. On re-save the plugin will delete (from the post meta) the old snippet, refetch the updated snippet from GitLab, and then store that new version in post meta again.


### USING THE SHORTCODE'S LINES ARG

Here's an example:
```
[wpez_gxes xid="2216308"  lines="2-4-7-_-10-20" ]
```

- xid = the ID of the GitLab snippet. GitHub isn't yet supported.

In ahort, the lines atts says, "Highlight these lines of code in snippet."

It's essential to understand that lines are actually a series of pairs concat'ed into a single string. 

Here's the breakdown of our example:

- 2-4 = Highlight all the lines from line 2 to line 4 (inclusive).
- 7-_ = Highlight line 7 to only line 7. The underscore is a flag to say "only one line is highlighted in this pair". That said, _,7 is not accepted syntax. 
- 10-20 = Highlight all the lines from line 10 to line 20 (inclusive).


### TODOs

- Shortcode code atts for using the native CSS.
- Add support for GitHub
- Revisit the other shortcode atts
- Min the JS.
- Is there a way to have tabs render like 4x spaces in the FE.