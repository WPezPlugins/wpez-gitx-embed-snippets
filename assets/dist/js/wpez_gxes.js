
document.addEventListener("DOMContentLoaded", function(){

    if (typeof WPEZ_GXES_LINES === 'object'){
        
        const wpezGxesJS = {};
        (function(ns){
            
            ns.init = function(){
                var wpezGxes = WPEZ_GXES_LINES;

                let btns = [], codes = [], hiLited = [], shareBtn = [];
                const glSnips = document.querySelectorAll(".GLab_wrapper");
                glSnips.forEach(function(glSnip ,index, arr){

                    let glID = glSnip.id;
                    let permaLink = wpezGxes.args.permalink;
                    let shareCopySuccess = wpezGxes.args.share_copy_success;
                    let shareCopyError = wpezGxes.args.share_copy_error;

                    // get all the snip's buttons
                    btns[glID] = glSnip.querySelectorAll(".line-numbers button")
                    // get all the snip's code
                    codes[glID] = glSnip.querySelectorAll(".code.highlight code > span");

                    if (typeof wpezGxes[glID] === 'string'){
                        // set the Highlights
                        ns.setHL(wpezGxes[glID], codes[glID]);
                        hiLited[glID] = wpezGxes[glID];
                    }
                    shareBtn[glID] = glSnip.querySelector('.wpez_gxes_share_btn');
                    shareBtn[glID].addEventListener('click', function(){
                        // TODO - test for hilited being empty. 
                        let shareURL = `${permaLink}?${glID}=${hiLited[glID]}#${glID}`;
                        // ref: https://developer.mozilla.org/en-US/docs/Web/API/Clipboard/writeText .
                        // This will break on a non secure site 
                        // https://stackoverflow.com/questions/51805395/navigator-clipboard-is-undefined

                        console.log( navigator.clipboard );
                        if (navigator.clipboard){
                            navigator.clipboard.writeText(shareURL).then(function() {
                                /* clipboard successfully set */
                                alert(shareCopySuccess);
                            }, function() {
                                /* clipboard write failed */
                                alert(shareCopyError);
                            });
                        } else {
                            alert( 'Oops! This feature only works when the site is served over HTTPS.' );
                        }
                    });

                    btns[glID].forEach(function(btn, ndx, arr){
                        btn.addEventListener('click', function(e){
                            // button click? toggle the corresponding code line
                            codes[glSnip.id][ndx].classList.toggle('wpez_gxes_hl');
                            // evaluted the highlighted lines
                            let pair = [], pairStart = false, pairEnd = '_', pairs = [];
                            codes[glSnip.id].forEach(function(code, ndx, arr ){
                                if (code.classList.contains('wpez_gxes_hl')){
                                    if (pairStart === false) {
                                        pairStart = ndx + 1;
                                    } else {
                                        pairEnd = ndx + 1;
                                    }
                                } else {
                                    if (pairStart !== false){
                                        pair = [pairStart, pairEnd];
                                        pairs.push(pair);
                                        pair = [];
                                        pairStart = false;
                                        pairEnd = '_';
                                    }
                                }
                            });
                            if (pairStart !== false){
                                pair = [pairStart, pairEnd];
                                pairs.push(pair);
                            }
                            hiLited[glSnip.id] = pairs.toString() ;
                        }, false);
                    });
                });
            }

            ns.setHL = function(strLine, nlistLines){
                
                const arrLines = ns.strLineToArray(strLine);
                for (let z=0; z < arrLines.length; z++){
                    if ( arrLines[z+1] === '' ) {
                        // TODO - check to make sure the ndx is valid. for example maybe it 20 and there are only 10 lines of code.
                        nlistLines[arrLines[z]-1].classList.toggle('wpez_gxes_hl')
                    } else if(arrLines[z] <= arrLines[z+1]){
                        for (let zz=arrLines[z]; zz <= arrLines[z+1]; zz++){
                            // TODO - see other TODO 
                            nlistLines[zz-1].classList.toggle('wpez_gxes_hl');
                        }
                    }
                    z++;
                }
            }

            ns.strLineToArray = function (strLine){
                // TODO - validate once converted?
                return strLine.split('-');
            }
        
        })(wpezGxesJS);
        wpezGxesJS.init();
      
    } else {
        console.log('WPEZ_GXES_LINES not available');
    }
});


