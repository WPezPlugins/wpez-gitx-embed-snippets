<?php
/**
 * Plugin Name: WPezPlugins: Git-X Embed Snippets
 * Plugin URI: TODO
 * Description: Embed GitHub or GitLab code snippets via shortcode. See README for benefits.
 * Version: 0.0.0 POC
 * Author: MF Simchock (Chief Executive Alchemist) for Alchemy United
 * Author URI: https://AlchemyUnited.com
 * License: GPLv2 or later
 * Text Domain: wpez_gxes
 *
 * @package WPezGitXEmbedSnippets
 */

namespace WPezGitXEmbedSnippets;

/**
 * Contains callbacks for save_post and add_shortcode.
 */
class Plugin {

	/**
	 * Shortcode name.
	 *
	 * @var string
	 */
	private $sc_name;


	/**
	 * Array used to keep a running list of data to "embed" with wp_add_inline_script().
	 *
	 * @var array
	 */
	private $arr_ais;


	/**
	 * The __construct sets the shortcode name.
	 *
	 * @param string $sc_name The shortcode name.
	 */
	public function __construct( string $sc_name = '' ) {

		$this->sc_name = trim( strtolower( $sc_name ) );
		$this->arr_ais = array();
	}


	/**
	 * The various settings are maintained here.
	 *
	 * @param string $key The key from the config array to return.
	 * @param string $default If the key is not set, then return this value.
	 *
	 * @return array|bool|string
	 */
	private function getConfig( $key = '', $default = '' ) {

		$arr_ex_post_types = array( 'attachment', 'nav_menu_item', 'revision' );
		$arr_temp          = apply_filters( __NAMESPACE__ . '\ExcludePostTypes', $arr_ex_post_types );
		if ( is_array( $arr_temp ) ) {
			$arr_ex_post_types = $arr_temp;
		}

		$sc_name = $this->sc_name;

		$arr = array(
			// TODO - don't use plugins_url(), it's not mu-plugins friendly.
			'plugin_url'         => trailingslashit( plugins_url( null, __FILE__ ) ),
			'plugin_folder_imgs' => 'assets/dist/img/',
			'exclude_post_types' => $arr_ex_post_types,
			'sc_name'            => $sc_name,
			'sc_name_'           => $sc_name . '_',
			'enqueue_js_handle'  => $sc_name,
			'enqueue_js_src'     => plugins_url( null, __FILE__ ) . '/assets/dist/js/wpez_gxes.js',
			'enqueue_js_ver'     => 'v0.0.0',
			'highlight_msg'      => 'Click line numbers to toggle highlighting.',
			'share_copy_success' => 'Success! The Share URL has been copied to your clipboard.',
			'share_copy_error'   => 'Oops. Copy to clipboard failed.',

			'gitx_args'          => array(
				'hub' => array(
					'active'            => false,
					'src'               => 'TODO',
					'kses_allowed_html' => 'TODO',
				),
				'lab' => array(
					'active'            => true,
					'src'               => 'https://gitlab.com/-/snippets/%s.js',
					'src_download_raw'  => 'https://gitlab.com/snippets/%s/raw?line_ending=raw',
					'id_prefix'         => 'GLab_',
					'assets'            => array(
						'logo'  => 'gitlab-logo.svg',
						'icons' => 'gitlab-sprite.png',
					),
					// https://developer.wordpress.org/reference/functions/wp_kses/ .
					'kses_allowed_html' => array(
						'a'       => array(
							'class'  => array(),
							'href'   => array(),
							'rel'    => array(),
							'target' => array(),
							'title'  => array(),
						),
						'article' => array(
							'class' => array(),
						),
						'button'  => array(
							'class' => array(),
							'type'  => array(),
						),
						'code'    => array(),
						'br'      => array(),
						'div'     => array(
							'class'        => array(),
							'data-path'    => array(),
							'data-type'    => array(),
							'data-blob-id' => array(),
							'id'           => array(),
							'role'         => array(),
						),
						'figure'  => array(
							'aria-label' => array(),
							'class'      => array(),
						),
						'img'     => array(
							'alt'   => array(),
							'class' => array(),
							'src'   => array(),
						),
						'pre'     => array(
							'class' => array(),
						),
						'span'    => array(
							'class' => array(),
							'id'    => array(),
							'lang'  => array(),

						),
						'small'   => array(),
						'strong'  => array(
							'class' => array(),
						),
					),
				),
			),
			'sc_atts_defaults'   => array(
				'active'  => true,
				'gitx'    => 'lab',
				'xid'     => false,
				'css'     => false,
				'lines'   => false,
				'wrapper' => true,
				'save'    => true,
			),

		);

		if ( isset( $arr[ $key ] ) ) {
			return $arr[ $key ];
		}
		return $default;
	}


	/**
	 * The crux of the admin-side half of the plugin.
	 *
	 * @param int     $post_id Post ID.
	 * @param WP_Post $obj_post Post object.
	 * @param bool    $bool_update Whether this is an existing post being updated.
	 *
	 * @return void
	 */
	public function savePost( $post_id, $obj_post, $bool_update ) {

		if ( ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) ) {
			return;
		}

		if ( ! ( $obj_post instanceof \WP_Post ) || in_array( $obj_post->post_type, $this->getConfig( 'exclude_post_types' ), true ) ) {
			return;
		}

		if ( wp_is_post_revision( $post_id ) ) {
			return;
		}

		$str_sc_name = $this->getConfig( 'sc_name' );
		$bool_has_sc = \has_shortcode( $obj_post->post_content, $str_sc_name );

		if ( true === $bool_has_sc ) {
			// https://developer.wordpress.org/reference/functions/get_shortcode_regex/ .
			$str_sc_regex = get_shortcode_regex();
			preg_match_all( '/' . $str_sc_regex . '/s', $obj_post->post_content, $arr_scs );

			// TODO - move $ndx to confix.
			$ndx = 3;
			if ( true === $this->arrCheck( $arr_scs, $ndx ) ) {

				$arr_atts_defaults = $this->getConfig( 'sc_atts_defaults' );
				// https://developer.wordpress.org/reference/functions/get_shortcode_atts_regex/ .
				$str_sc_atts_regex = get_shortcode_atts_regex();
				// loop over all the shortcodes.
				foreach ( $arr_scs[ $ndx ] as $i => $str_sc ) {

					preg_match_all( $str_sc_atts_regex, $str_sc, $arr_sc_atts );
					if ( true === $this->arrCheck( $arr_sc_atts, 1 ) && true === $this->arrCheck( $arr_sc_atts, 2 ) ) {

						$arr_atts_map = array();
						foreach ( $arr_sc_atts[1] as $ii => $str_sc_att ) {
							$arr_atts_map[ trim( $str_sc_att ) ] = $arr_sc_atts[2][ $ii ];
						}
						// get the sc atts and merge the defaults "under" them.
						$arr_atts_ready = array_merge( $arr_atts_defaults, $arr_atts_map );

						$arr_atts_ready['xid'] = trim( $arr_atts_ready['xid'] );
						// some validation(s) .
						if ( false === $this->checkActive( $arr_atts_ready ) ) {
							continue;
						}
						if ( false === $this->checkSave( $arr_atts_ready ) ) {
							continue;
						}
						if ( false === $this->checkOther( $arr_atts_ready ) ) {
							continue;
						}

						$arr_atts_ready['gitx'] = strtolower( $arr_atts_ready['gitx'] );
						$str_url                = $this->getRemoteURL( $arr_atts_ready );
						$mix_get_remote         = $this->getRemote( $str_url );
						// we get_remote...is it good?
						if ( false === $mix_get_remote || empty( $mix_get_remote ) ) {
							continue;
						}
						$str_gitx = $arr_atts_ready['gitx'];
						// begins the breakingdown of the get_remote.
						$arr_pgr = $this->parseGetRemote( $mix_get_remote, $str_gitx );
						// continues the breakdown.
						$arr_pgr = $this->prepArrayForUpdatePostMeta( $arr_pgr, $str_gitx );
						// get the URI for the css.
						$arr_pgr['css'] = $this->parseLinkStylesheet( $arr_pgr['css'], $str_gitx );
						// use that URI to get the actual CSS.
						$arr_pgr['css'] = $this->getRemote( $arr_pgr['css'] );
						// clean up the CSS.
						$arr_pgr['css'] = $this->cleanCSS( $arr_pgr['css'], $str_gitx );
						// clean up the markup.
						$arr_pgr['markup'] = $this->cleanMarkup( $arr_pgr['markup'], $str_gitx, $arr_atts_ready['xid'] );
						// do we have highlight lines?.
						$arr_pgr['lines'] = false;
						if ( isset( $arr_atts_ready['lines'] ) && ! empty( $arr_atts_ready['lines'] ) && is_string( $arr_atts_ready['lines'] ) ) {
							$arr_pgr['lines'] = $this->cleanLines( $str_gitx, $arr_atts_ready['lines'] );
						}
						// get the post_meta_name.
						$str_pm_name = $this->getPostMetaName( $arr_atts_ready );
						// store the snippet's 'css' and 'markup' in a post_meta row.
						// each snippet gets its own row. the same snipper ID will be stored once (for now).
						// Note: This means that if you update the snippet you'll have to re-saec the WP post as well (for now).
						$upm = update_post_meta( $post_id, $str_pm_name, $arr_pgr );
					}
				}
			}
		}
	}

	/**
	 * Take the sting for the highlighting lines and cleans it up (read: validation).
	 *
	 * @param string $str_gitx What Git platform.
	 * @param string $str_lines The comma delimited string representing the array of lines to be highlighted.
	 *
	 * @return string
	 */
	protected function cleanLines( string $str_gitx, string $str_lines ) {

		// TODO - add to config and pass to JS.
		$delimiter = '-';

		// get rid of any spaces.
		$str_lines = \str_replace( ' ', '', $str_lines );
		$arr_lines = explode( $delimiter, $str_lines );

		$arr_lines_clean = array();
		$y               = count( $arr_lines ) - 1;
		for ( $x = 0; $x <= $y; $x++ ) {
			// TODO - check for #s being sequential.
			if ( filter_var( $arr_lines[ $x ], FILTER_VALIDATE_INT ) && ( filter_var( $arr_lines[ $x + 1 ], FILTER_VALIDATE_INT ) || '_' === $arr_lines[ $x + 1 ] ) ) {

				$arr_lines_clean[] = $arr_lines[ $x ];
				$arr_lines_clean[] = $arr_lines[ $x + 1 ];
			}
			// we check in pairs, so we want ++ before getting back to the for().
			$x++;
		}

		// back to a string.
		$str_lines_clean = implode( $delimiter, $arr_lines_clean );
		return $str_lines_clean;
	}

	/**
	 * Every time we save pst we want to re-evaluate our Gitx embeds. Maybe one was deleted? Or updated in some way?
	 *
	 * @param int   $post_id Post ID.
	 * @param array $arr_data Array of unslashed post data.
	 *
	 * @return void
	 */
	public function resetPostMeta( $post_id, $arr_data ) {

		$obj_post = get_post( $post_id );
		if ( ! ( $obj_post instanceof \WP_Post ) || in_array( $obj_post->post_type, $this->getConfig( 'exclude_post_types' ), true ) ) {
			return;
		}

		$str_sc_name = $this->getConfig( 'sc_name' );
		global $wpdb;
		// https://developer.wordpress.org/reference/classes/wpdb/esc_like/ .
		$like_lab = $wpdb->esc_like( $str_sc_name . '_lab_' ) . '%';
		$like_hub = $wpdb->esc_like( $str_sc_name . '_hub_' ) . '%';
		$results  = $wpdb->get_results( $wpdb->prepare( "DELETE FROM {$wpdb->prefix}postmeta WHERE post_id = %s AND ( meta_key LIKE %s OR meta_key LIKE %s )", $post_id, $like_lab, $like_hub ) );
	}

	/**
	 * Replaces the original assets with local versions.
	 *
	 * @param string $str_css The CSS to process.
	 * @param string $str_gitx The gitx source.
	 *
	 * @return string
	 */
	private function cleanCSS( string $str_css = '', string $str_gitx = 'lab' ) {

		$arr_gitx_args = $this->getConfig( 'gitx_args' );

		switch ( $str_gitx ) {
			case 'hub':
				// TODO.
				break;
			case 'lab':
				if ( ! isset( $arr_gitx_args['lab']['assets']['icons'] ) || ! is_string( $arr_gitx_args['lab']['assets']['icons'] ) ) {
					return $str_css;
				}
				$str_icons = $arr_gitx_args['lab']['assets']['icons'];
				// TODO - move regex pattern into config.
				preg_match( '/\.gitlab-embed-snippets.+?url\("([^"]*)"/', $str_css, $output_array );
				if ( isset( $output_array[1] ) && ! empty( $output_array[1] ) && is_string( $output_array[1] ) ) {
					// TODO - esc / validate this URL.
					$str_new_icons = $this->getConfig( 'plugin_url' ) . $this->getConfig( 'plugin_folder_imgs' ) . $str_icons;
					if ( false === filter_var( $str_new_icons, FILTER_VALIDATE_URL ) ) {
						return $str_css;
					}
					$str_css = \str_replace( $output_array[1], $str_new_icons, $str_css );
					return $str_css;
				}
				return '';
				break;
			default:
				return '';
		}
	}

	/**
	 * Undocumented function
	 *
	 * @param string $str_gitx The Git platform.
	 * @param string $str_xid The platform's ID for the snippet.
	 *
	 * @return string
	 */
	private function getID( string $str_gitx = 'lab', string $str_xid = '' ) {

		$arr_gitx_args = $this->getConfig( 'gitx_args' );

		if ( 'lab' === strtolower( $str_gitx ) ) {

			$str_prefix = trim( $arr_gitx_args['lab']['id_prefix'] );
			return $str_prefix . trim( $str_xid );
		}
	}

	/**
	 *  Replaces the original assets with local versions.
	 *
	 * @param string $str_html The HTMLto be processed.
	 * @param string $str_gitx The gitx source.
	 * @param string $str_xid  The platform's snippet ID.
	 *
	 * @return string
	 */
	private function cleanMarkup( string $str_html = '', string $str_gitx = 'lab', string $str_xid = '' ) {

		$arr_gitx_args = $this->getConfig( 'gitx_args' );

		switch ( $str_gitx ) {
			case 'hub':
				// TODO.
				break;

			case 'lab':
				// Note: I did not use DocumwentCOM 'cause it throws warnings for HTML5 tags, maybe later.
				// For now - as a POC - this will do.

				if ( ! isset( $arr_gitx_args['lab']['assets']['logo'] ) || ! is_string( $arr_gitx_args['lab']['assets']['logo'] ) ) {
					return $str_html;
				}
				$str_logo = $arr_gitx_args['lab']['assets']['logo'];
				// TODO - move regex pattern into config.
				preg_match( '/<img.+?src="([^"]*)".*?\/?>/', $str_html, $output_array );
				if ( isset( $output_array[1] ) && ! empty( $output_array[1] ) && is_string( $output_array[1] ) ) {
					$str_new_logo = $this->getConfig( 'plugin_url' ) . $this->getConfig( 'plugin_folder_imgs' ) . $str_logo;
					if ( false !== filter_var( $str_new_logo, FILTER_VALIDATE_URL ) ) {
						// replace the GL.com logo svg with our plugin version.
						$str_html = \str_replace( $output_array[1], $str_new_logo, $str_html );
					}
				}
				// add the highlight_msg.
				$str_html = \str_replace( '<figure ', '<div class="glab-highlight-msg">' . esc_attr( $this->getConfig( 'highlight_msg' ) ) . '</div><figure ', $str_html );

				// this element.
				$str_logo_wrapper_pattern = '/<a\s[^>]*class\s*=\s*\"gitlab-logo-wrapper\".*?><img.*?class\s*=\s*\"gitlab-logo\".*?><\/a>/';
				preg_match( $str_logo_wrapper_pattern, $str_html, $output_array2 );

				if ( isset( $output_array2[0] ) && is_string( $output_array2[0] ) && ! empty( $output_array2[0] ) ) {

					$str_logo_wrapper = $output_array2[0];
					// remove it.
					$str_html = \str_replace( $str_logo_wrapper, '', $str_html );
					// update the title.
					$str_logo_wrapper = \str_replace( ' title="View on GitLab"', ' title="View on GitLab.com in a new tab"', $str_logo_wrapper );
					// this div.
					$temp = '<div class="file-header-content">';
					// now move it.
					$str_html = \str_replace( $temp, $str_logo_wrapper . $temp, $str_html );
				}
				// this <div>.
				$str_file_header_content_pattern = '/<div\s[^>]*class\s*=\s*\"file-header-content\".*?\/small><\/div>/';
				preg_match( $str_file_header_content_pattern, $str_html, $output_array3 );

				if ( isset( $output_array3[0] ) && is_string( $output_array3[0] ) && ! empty( $output_array3[0] ) ) {
										// remove it.
					$str_html = \str_replace( $output_array3[0], '', $str_html );

					// this link.
					$str_embedded_snippet_title_pattern = '/<a.*?class\s*=\s*\"gitlab-embedded-snippets-title\".*?>([\s\S]*)<\/a>/';
					// from in the previous dev.
					preg_match( $str_embedded_snippet_title_pattern, $output_array3[0], $output_array4 );

					if ( isset( $output_array4[0], $output_array4[1] ) ) {

						// change the "button" anchor text.
						$str_temp_a = \str_replace( trim( $output_array4[1] ), 'On GitLab', $output_array4[0] );
						// change the class and add a title.
						$str_temp_a = \str_replace( 'gitlab-embedded-snippets-title"', 'gl-button btn btn-default"  title="Open ' . esc_attr( trim( $output_array4[1] ) ) . ' on GitLab.com in a new tab" ', $str_temp_a );
						// move it within the btn-group wrapper (with the other 2 links / buttons).
						$str_html = \str_replace( '<div class="btn-group" role="group">', '<div class="btn-group" role="group">' . $str_temp_a, $str_html );
					}
				}

				$str_html = \str_replace( '<div class="btn-group" role="group">', '<div class="btn-group" role="group"><button class="wpez_gxes_share_btn">Share URL</button>', $str_html );

				// this <div> - we're going to rework the line numbers (into buttons).
				$str_line_numbers_pattern = '/<div\s[^>]*class\s*=\s*\"line-numbers\".*?span><\/div>/';
				preg_match( $str_line_numbers_pattern, $str_html, $output_array5 );

				if ( isset( $output_array5[0] ) ) {

					// change the "button" anchor text.
					$str_temp_a = \str_replace( 'line-numbers', 'line-numbers diff-line-num', $output_array5[0] );
					$str_temp_a = \str_replace( '<span class="diff-line-num">', '<button type="button">', $str_temp_a );
					$str_temp_a = \str_replace( '</span>', '</button>', $str_temp_a );
					$str_html   = \str_replace( $output_array5[0], $str_temp_a, $str_html );
				}

				// remove this span. 
				$str_html = \str_replace( '<span class="gl-snippet-icon gl-snippet-icon-doc-text"></span>', '', $str_html );
				// remove: <span class="gl-snippet-icon gl-snippet-icon-copy-to-clipboard"></span>
				$str_html = \str_replace( '<span class="gl-snippet-icon gl-snippet-icon-copy-to-clipboard"></span>', '', $str_html );

				// remove: <button class="gl-button btn btn-default copy-to-clipboard-btn"></button>
				$str_html = \str_replace( '<button class="gl-button btn btn-default copy-to-clipboard-btn"></button>', '', $str_html );

				$str_copy_to_clip_pattern = '/<button\s[^>]*class\s*=\s*("|"([^"]*)\s)copy-to-clipboard-btn("|\s([^"]*)").*?<\/button>/';
				$str_html = preg_replace( $str_copy_to_clip_pattern, '', $str_html );

				// change the open raw title.
				$str_html = \str_replace( ' title="Open raw" ', ' title="Open raw on GitLab.com in a new tab" ', $str_html );
				// replace the open raw span with anchor text.
				$str_html = \str_replace( '<span class="gl-snippet-icon gl-snippet-icon-doc-code"></span>', 'Open Raw', $str_html );
				// replace the download span with anchor text.
				$str_html = \str_replace( '<span class="gl-snippet-icon gl-snippet-icon-download"></span>', 'Download', $str_html );
				// update the title for the Download link.
				$str_html = \str_replace( ' title="Download"', ' title="Download from GitLab.com"', $str_html );

				$str_xid = trim( $str_xid );
				// Let's (try to) make the code id= more unique.
				if ( ! empty( $str_xid ) && ! empty( esc_attr( $str_xid ) ) ) {
					$str_xid = trim( $str_xid );
					$str_html = \str_replace( '<span id="LC', '<span id="GLab-' . \esc_attr( $str_xid ) . '-', $str_html );
				}
				$str_html = \str_replace( ' class="line" ', ' ', $str_html );

				return '<div id="' . esc_attr( $this->getID( $str_gitx, $str_xid ) ) . '" class="GLab_wrapper">' . $str_html . '</div>';
				break;

			default:
				return '';
		}
	}

	/**
	 * Parses out the URL for the CSS.
	 *
	 * @param string $str_link The string with the CSS URL in it.
	 * @param string $str_gitx The gitx source.
	 *
	 * @return string
	 */
	private function parseLinkStylesheet( string $str_link = '', string $str_gitx = 'lab' ) {

		$str_ret = '';

		switch ( $str_gitx ) {
			case 'hub':
				// TODO.
				break;
			case 'lab':
				// TODO - move regex pattern into config.
				preg_match( '/href=\\"(.*).css/', $str_link, $output_array );
				if ( isset( $output_array[1] ) && ! empty( $output_array[1] ) && is_string( $output_array[1] ) ) {

					$str_ret = $output_array[1] . '.css';
				}
				break;
			default:
				// TODO.
		}
		return $str_ret;
	}

	/**
	 * Splits the result of the get remote into pieces.
	 *
	 * @param string $str_get_remote The get remote string.
	 * @param string $str_gitx       The gitx source.
	 *
	 * @return array
	 */
	private function parseGetRemote( string $str_get_remote = '', string $str_gitx = 'lab' ) {

		$output_array = array();

		switch ( $str_gitx ) {
			case 'hub':
				// TODO.
				break;
			case 'lab':
			default:
				// TODO - move regex pattern into config.
				preg_match_all( '/document.write\(\'(.*?)\'\);/', $str_get_remote, $output_array );
		}
		return $output_array;
	}

	/**
	 * Further processes the results from parseGetRemote().
	 *
	 * @param array  $arr_pgr The results from parseGetRemote().
	 * @param string $str_gitx The gitx source.
	 *
	 * @return array
	 */
	private function prepArrayForUpdatePostMeta( array $arr_pgr = array(), $str_gitx = 'lab' ) {

		switch ( $str_gitx ) {

			case 'hub':
				// TODO.
				break;
			case 'lab':
			default:
				if ( isset( $arr_pgr[0][0], $arr_pgr[0][1], $arr_pgr[1][0], $arr_pgr[1][1] ) ) {
					$arr_pgr['css']    = $this->reformatDocumentWrite( $arr_pgr[1][0] );
					$arr_pgr['markup'] = $this->reformatDocumentWrite( $arr_pgr[1][1] );
					unset( $arr_pgr[0] );
					unset( $arr_pgr[1] );
				} else {
					// TODO?
				}
		}
		return $arr_pgr;
	}


	/**
	 * Takes a JS document.write string and makes it PHP friendly.
	 *
	 * @param string $str_dw The string to be converted.
	 *
	 * @return string
	 */
	private function reformatDocumentWrite( string $str_dw = '' ) {

		// In the code column of content, there might be blank lines, we need to replace those \n with <br> before we replace all the other \n. 
		$str = str_replace( '><\/span>\n<span id', '><\/span><br><span id', $str_dw );
		$str = str_replace( '\n', '', $str);
		$str = str_replace( '\\"', '"', $str );
		$str = str_replace( '<\\/', '</', $str );

		return $str;
	}

	/**
	 * Checks if a particular array index is an array itself and not empty.
	 *
	 * @param array  $arr Array to be checked.
	 * @param string $ndx The index to be checked.
	 *
	 * @return bool
	 */
	private function arrCheck( array $arr = array(), string $ndx = '' ) {

		if ( isset( $arr[ $ndx ] ) && is_array( $arr[ $ndx ] ) && ! empty( $arr[ $ndx ] ) ) {
			return true;
		}
		return false;
	}

	/**
	 * Is the index 'active' true.
	 *
	 * @param array $arr_atts_ready Array of shortcode atts.
	 *
	 * @return bool
	 */
	private function checkActive( array $arr_atts_ready ) {

		if ( true !== $arr_atts_ready['active'] && 'true' !== $arr_atts_ready['active'] ) {
			return false;
		}
		return true;
	}

	/**
	 * Is the index 'save' true.
	 *
	 * @param array $arr_atts_ready Array of shortcode atts.
	 *
	 * @return bool
	 */
	private function checkSave( array $arr_atts_ready ) {

		if ( true !== $arr_atts_ready['save'] && 'true' !== $arr_atts_ready['save'] ) {
			return false;
		}
		return true;
	}


	/**
	 * Checks the other atts.
	 *
	 * @param array $arr_atts_ready Array of shortcode atts.
	 *
	 * @return bool
	 */
	private function checkOther( array $arr_atts_ready ) {

		$arr_gitx_args = $this->getConfig( 'gitx_args', array() );

		$str_gitx = strtolower( trim( $arr_atts_ready['gitx'] ) );
		if ( ! isset( $arr_gitx_args[ $str_gitx ] ) || true !== $arr_gitx_args[ $str_gitx ]['active'] ) {
			return false;
		}

		if ( false === $arr_atts_ready['xid'] || false === ctype_digit( $arr_atts_ready['xid'] ) ) {
			return false;
		}

		return true;
	}


	/**
	 * Builds the remote URL.
	 *
	 * @param array $arr_atts_ready The array of atts from the shortcode.
	 *
	 * @return string
	 */
	private function getRemoteURL( array $arr_atts_ready = array() ) {

		$str_gitx      = $arr_atts_ready['gitx'];
		$arr_gitx_args = $this->getConfig( 'gitx_args', array() );
		if ( isset( $arr_gitx_args[ $str_gitx ]['src'] ) ) {
			$str_src = $arr_gitx_args[ $str_gitx ]['src'];
			$str_id  = $arr_atts_ready['xid'];
			$str_url = sprintf( $str_src, esc_attr( $str_id ) );
			return $str_url;
		}
		return '';
	}


	/**
	 * Wrapper for wp_remote_get() and wp_remote_retrieve_body().
	 *
	 * @param string  $str_url URL to retrieve.
	 * @param boolean $bool_body Retrieve only the body from the raw response.
	 *
	 * @return array|bool|string
	 */
	private function getRemote( string $str_url = '', bool $bool_body = true ) {

		if ( false === filter_var( $str_url, FILTER_VALIDATE_URL ) ) {
			return false;
		}

		// https://developer.wordpress.org/reference/functions/wp_remote_get/ .
		$wp_remote_get = \wp_remote_get( $str_url );

		if ( $wp_remote_get instanceof \WP_Error ) {
			return false;
		}
		if ( true === $bool_body ) {
			// https://developer.wordpress.org/reference/functions/wp_remote_retrieve_body/ .
			return wp_remote_retrieve_body( $wp_remote_get );
		}
		return $wp_remote_get;
	}

	/**
	 * Standardized build of the post meta name.
	 *
	 * @param array $arr_atts_ready The array of atts from the shortcode.
	 *
	 * @return string
	 */
	private function getPostMetaName( array $arr_atts_ready = array() ) {

		$str_sc_name = $this->getConfig( 'sc_name_' );
		$str         = $str_sc_name . trim( $arr_atts_ready['gitx'] ) . '_' . trim( $arr_atts_ready['xid'] );
		return $str;
	}


	/**
	 * The add_shortcode() callback.
	 *
	 * @param array       $atts The sc attributes.
	 * @param string|null $sc_content What's between the [sc]...[/sc].
	 * @param string      $sc_name Shortcode name.
	 *
	 * @return string
	 */
	public function addShortcode( $atts, $sc_content, $sc_name ) {

		static $arr_x_cnt = array();

		$arr_atts_defaults = $this->getConfig( 'sc_atts_defaults', array() );

		$arr_atts = shortcode_atts(
			$arr_atts_defaults,
			$atts
		);

		if ( false === $this->checkActive( $arr_atts ) ) {
			return '';
		}

		if ( false === $this->checkOther( $arr_atts ) ) {
			return '';
		}

		if ( false === $this->checkSave( $arr_atts ) ) {

			// TODO - Test this!
			$str_url        = $this->getRemoteURL( $arr_atts );
			$mix_get_remote = $this->getRemote( $str_url );
			if ( false === $mix_get_remote ) {
				return '';
			}
			$arr_x_cnt[ $arr_atts['gitx'] ] = true;
			return '<script>' . $mix_get_remote . '</script>';
		}

		global $post;
		$str_pm_name = $this->getPostMetaName( $arr_atts );
		$arr_embed   = get_post_meta( $post->ID, $str_pm_name, true );

		if ( ! is_array( $arr_embed ) || ! isset( $arr_embed['css'], $arr_embed['markup'], ) ) {
			return '';
		}
		$arr_gitx_args = $this->getConfig( 'gitx_args' );

		if ( ! isset( $arr_gitx_args[ $arr_atts['gitx'] ]['kses_allowed_html'] ) || ! \is_array( $arr_gitx_args[ $arr_atts['gitx'] ]['kses_allowed_html'] ) ) {
			return '';
		}
		$arr_kses = $arr_gitx_args[ $arr_atts['gitx'] ]['kses_allowed_html'];

		// TODO - Test this!
		if ( ( ! isset( $arr_x_cnt[ $arr_atts['gitx'] ] ) || true !== $arr_x_cnt[ $arr_atts['gitx'] ] ) && false !== $arr_atts['css'] ) {
			$arr_x_cnt[ $arr_atts['gitx'] ] = true;
			return '<style>' . wp_strip_all_tags( $arr_embed['css'] ) . '</style>' . \wp_kses( $arr_embed['markup'], $arr_kses );
		}

		// Do we have a share override for the highlighted lines of code?
		$str_lines = '';
		if ( is_string( $arr_embed['lines'] ) ) {
			$str_lines = $arr_embed['lines'];
		}
		$str_get_this = $this->getID( $arr_atts['gitx'], $arr_atts['xid'] );
		if ( isset( $_GET[ $str_get_this ] ) ) {

			$str_clean_lines = $this->cleanLines( $arr_atts['gitx'], wp_unslash( $_GET[ $str_get_this ] ) );
			if ( ! empty( $str_clean_lines ) ) {
				$str_lines = $str_clean_lines;
			}
		}

		// Add to the ais (add inline script).
		$this->arr_ais[ $this->getID( $arr_atts['gitx'], $arr_atts['xid'] ) ] = $str_lines;

		return \wp_kses( $arr_embed ['markup'], $arr_kses );
	}


	/**
	 * Enqueue the JS script.
	 *
	 * @return void
	 */
	public function enqueueScripts() {

		global $post;

		$str_sc_name = $this->getConfig( 'sc_name' );

		// Only enqueue the script if we have our shortcode in the content.
		if ( has_shortcode( $post->post_content, $str_sc_name ) ) {

			wp_enqueue_script(
				$this->getConfig( 'enqueue_js_handle' ),
				$this->getConfig( 'enqueue_js_src' ),
				array(),
				$this->getConfig( 'enqueue_js_ver' ),
				true,
			);
		}
	}

	public function addInlineScript() {

		$str_sc_name = $this->getConfig( 'sc_name' );
		$str_sc_name = trim( $str_sc_name );

		$this->arr_ais['args']['permalink']          = \esc_url( get_permalink( get_the_ID() ) );
		$this->arr_ais['args']['share_copy_success'] = \esc_attr( $this->getConfig( 'share_copy_success' ) );
		$this->arr_ais['args']['share_copy_error']   = \esc_attr( $this->getConfig( 'share_copy_error' ) );

		wp_add_inline_script( $this->getConfig( 'enqueue_js_handle' ), 'const ' . strtoupper( $str_sc_name ) . '_LINES=' . wp_json_encode( $this->arr_ais ), 'before' );
	}

}


function initPlugin() {

	$str_sc_name = 'wpez_gxes';

	static $new;

	if ( null === $new ) {
		$new = new Plugin( $str_sc_name );
	}

	// This is done to "compensate" for "timing issues" that let's the plugin work its magic.
	if ( 'init' === current_action() ) {
		add_shortcode( $str_sc_name, array( $new, 'addShortcode' ) );
		add_action( 'pre_post_update', array( $new, 'resetPostMeta' ), 1, 2 );
		add_action( 'wp_enqueue_scripts', array( $new, 'enqueueScripts' ) );
		// yeah, odd hook ;) but we need to wait after content setup but before enqueue'ing.
		add_action( 'get_sidebar', array( $new, 'addInlineScript' ), 1000 );
	} elseif ( 'load-post.php' === current_action() ) {
		add_action( 'save_post', array( $new, 'savePost' ), 500, 3 );
	}
}
// https://wordpress.stackexchange.com/questions/22776/plugin-development-wordpress-processes-twice-on-post-update-how-to-skip-proces .

add_action( 'init', __NAMESPACE__ . '\initPlugin', 100 );
add_action( 'load-post.php', __NAMESPACE__ . '\initPlugin' );
